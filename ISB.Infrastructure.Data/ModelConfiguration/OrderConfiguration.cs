﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ISB.Domain.Model;

namespace ISB.Infrastructure.DAL.ModelConfiguration
{
    public class OrderConfiguration : EntityTypeConfiguration<Order>
    {
        public OrderConfiguration()
        {
            HasKey(i => i.OrderId);

            Property(i => i.OrderId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(i => i.VehicleType).IsRequired();
            Property(i => i.BoxNumber).IsRequired();
            Property(i => i.BeginOrderDateTime).IsRequired();
            Property(i => i.EndOrderDateTime).IsRequired();
            Property(i => i.WorksSet).IsRequired();
            Property(i => i.RaisedDateTime).IsRequired();
            Property(i => i.OrderStatus).IsRequired();

            ToTable("Orders");  
        }
    }
}
