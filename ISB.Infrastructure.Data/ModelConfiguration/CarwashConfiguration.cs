﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ISB.Domain.Model;

namespace ISB.Infrastructure.DAL.ModelConfiguration
{
    public class CarwashConfiguration : EntityTypeConfiguration<Carwash>
    {
        public CarwashConfiguration()
        {
            HasKey(i => i.CarwashId);

            Property(i => i.CarwashId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(i => i.BoxQuantity).IsRequired();
            Property(i => i.CarwashName).IsRequired();
            Property(i => i.Address).IsRequired();
            Property(i => i.Lat).IsRequired();
            Property(i => i.Lon).IsRequired();
            Property(i => i.CompanyName).IsOptional();
            Property(i => i.PhoneNumber).IsRequired();
            Property(i => i.BeginDatetime).IsRequired();
            Property(i => i.EndDatetime).IsRequired();
            Property(i => i.ImagePath).IsOptional();

            HasMany(i => i.Orders)
                .WithRequired(i => i.Carwash)
                .HasForeignKey(i => i.CarwashId)
                .WillCascadeOnDelete();

            HasMany(i => i.ConcreteCarwashWorks)
                .WithRequired(i => i.Carwash)
                .HasForeignKey(i => i.CarwashId)
                .WillCascadeOnDelete();

            ToTable("Carwashes");
        }
    }
}
