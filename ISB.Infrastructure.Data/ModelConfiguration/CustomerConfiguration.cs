﻿using System.Data.Entity.ModelConfiguration;
using ISB.Domain.Model;

namespace ISB.Infrastructure.DAL.ModelConfiguration
{
    public class CustomerConfiguration : EntityTypeConfiguration<Сustomer>
    {
        public CustomerConfiguration()
        {
            HasKey(i => i.CustomerId);

            Property(i => i.FirstName).IsOptional();
            Property(i => i.GosNumber).IsRequired();
            Property(i => i.LastName).IsOptional();
            Property(i => i.PhoneNumber).IsRequired();

            HasMany(i => i.Orders)
                .WithOptional();//.HasForeignKey(i => i.CustomerId);
                
            ToTable("Customers");   
        }
    }
}
