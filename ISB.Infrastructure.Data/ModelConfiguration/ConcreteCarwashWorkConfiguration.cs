﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ISB.Domain.Model;

namespace ISB.Infrastructure.DAL.ModelConfiguration
{
    class ConcreteCarwashWorkConfiguration : EntityTypeConfiguration<ConcreteCarwashWork>
    {
        public ConcreteCarwashWorkConfiguration()
        {
            HasKey(i => i.ConcreteCarwashWorkId);

            //HasOptional(i => i.Carwash)
            //   .WithMany()
            //   .HasForeignKey(i => i.CarwashId);
            //HasRequired(i => i.Carwash)
            //    .WithMany()
            //    .HasForeignKey(i => i.CarwashId);

            Property(i => i.ConcreteCarwashWorkId)
                           .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(i => i.Duration);
            Property(i => i.Cost);

            ToTable("ConcreteCarwashWorks");
        }
    }
}
