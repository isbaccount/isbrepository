﻿using System.Data.Objects;

namespace ISB.Infrastructure.DAL
{
    public interface IDatabaseFactory
    {
        ISBContext Get();
        ObjectContext GetObjectContext();
    }
}
