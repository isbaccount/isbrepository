﻿using System;
using System.Linq.Expressions;

namespace ISB.Infrastructure.DAL.Interfaces
{
    public interface ISpecification<TEntity>
    {
        /// <summary>
        /// Select/Where Expression
        /// </summary>
        Expression<Func<TEntity, bool>> EvalPredicate { get; }
        /// <summary>
        /// Function to evaluate where Expression
        /// </summary>
        Func<TEntity, bool> EvalFunc { get; }
    }
}
