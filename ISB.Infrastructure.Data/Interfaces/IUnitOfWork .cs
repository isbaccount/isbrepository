﻿using System;
using System.Data.Common;

namespace ISB.Infrastructure.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TSet> GetRepository<TSet>() where TSet : class;
        DbTransaction BeginTransaction();
        int Save();
    }
}
