﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Objects;
using System.Linq.Expressions;

namespace ISB.Infrastructure.DAL.Interfaces
{
    /// <summary>
    /// Repository Interface defines the base
    /// functionality required by all Repositories.
    /// </summary>
    /// <typeparam name="TEntity">
    /// The entity type that requires a Repository.
    /// </typeparam>
    public interface IRepository<TEntity>
    {
        string KeyProperty { get; set; }

        void Add(TEntity entity);
        void AddOrAttach(TEntity entity);
        void DeleteRelatedEntries(TEntity entity);
        void DeleteRelatedEntries
        (TEntity entity, ObservableCollection<string> keyListOfIgnoreEntites);
        void Delete(TEntity entity);

        ObjectQuery<TEntity> DoQuery();
        ObjectQuery<TEntity> DoQuery(ISpecification<TEntity> where);
        ObjectQuery<TEntity> DoQuery(int maximumRows, int startRowIndex);
        ObjectQuery<TEntity> DoQuery(Expression<Func<TEntity, object>> sortExpression);
        ObjectQuery<TEntity> DoQuery(Expression<Func<TEntity, object>> sortExpression,
                    int maximumRows, int startRowIndex);

        IList<TEntity> SelectAll(string entitySetName);
        IList<TEntity> SelectAll();
        IList<TEntity> SelectAll(string entitySetName, ISpecification<TEntity> where);
        IList<TEntity> SelectAll(ISpecification<TEntity> where);
        IList<TEntity> SelectAll(int maximumRows, int startRowIndex);
        IList<TEntity> SelectAll(Expression<Func<TEntity, object>> sortExpression);
        IList<TEntity> SelectAll(Expression<Func<TEntity, object>> sortExpression,
                    int maximumRows, int startRowIndex);

        TEntity SelectByKey(string key);

        bool TrySameValueExist(string fieldName, object fieldValue, string key);
        bool TryEntity(ISpecification<TEntity> selectSpec);

        int GetCount();
        int GetCount(ISpecification<TEntity> selectSpec);
    }
}
