﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ISB.Domain.Model;
using ISB.Infrastructure.DAL.ModelConfiguration;

namespace ISB.Infrastructure.DAL
{
    public class ISBContext : DbContext
    {
        public ISBContext(string connectionStringName)
            : base(connectionStringName)
        {
        }

        public DbSet<Carwash> Carwashes { get; set; }
        public DbSet<ConcreteCarwashWork> ConcreteCarwashWorks { get; set; }
        public DbSet<Сustomer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //modelBuilder.Conventions.Remove<IncludeMetadataConvention>();

            modelBuilder.Configurations.Add(new CarwashConfiguration());
            modelBuilder.Configurations.Add(new ConcreteCarwashWorkConfiguration());
            modelBuilder.Configurations.Add(new CustomerConfiguration());
            modelBuilder.Configurations.Add(new OrderConfiguration());
        }
    }
}