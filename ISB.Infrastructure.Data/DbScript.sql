create table [dbo].[Carwashes] (
    [CarwashId] [uniqueidentifier] not null,
    [Name] [nvarchar](max) null,
    [BoxQuantity] [int] not null,
    primary key ([CarwashId])
);
create table [dbo].[ConcreteCarwashWorks] (
    [ConcreteCarwashWorkId] [uniqueidentifier] not null,
    [CarwashId] [uniqueidentifier] not null,
    [WorkType] [int] not null,
    [Cost] [real] not null,
    [Duration] [time](7) not null,
    primary key ([ConcreteCarwashWorkId])
);
create table [dbo].[Orders] (
    [OrderId] [uniqueidentifier] not null,
    [СustomerId] [uniqueidentifier] not null,
    [CarwashId] [uniqueidentifier] not null,
    [VehicleType] [int] not null,
    [BoxNumber] [int] not null,
    [OrderDateTime] [datetime] not null,
    [WorksSet] [int] not null,
    [RaisedDateTime] [datetime] not null,
    [Сustomer_CustomerId] [uniqueidentifier] null,
    primary key ([OrderId])
);
create table [dbo].[Customers] (
    [CustomerId] [uniqueidentifier] not null,
    [GosNumber] [nvarchar](max) null,
    [FirstName] [nvarchar](max) null,
    [LastName] [nvarchar](max) null,
    [Orders_OrderId] [uniqueidentifier] null,
    primary key ([CustomerId])
);
alter table [dbo].[ConcreteCarwashWorks] add constraint [Carwash_ConcreteCarwashWorks] foreign key ([CarwashId]) references [dbo].[Carwashes]([CarwashId]) on delete cascade;
alter table [dbo].[Orders] add constraint [Carwash_Orders] foreign key ([CarwashId]) references [dbo].[Carwashes]([CarwashId]) on delete cascade;
alter table [dbo].[Orders] add constraint [Order_Сustomer] foreign key ([Сustomer_CustomerId]) references [dbo].[Customers]([CustomerId]);
alter table [dbo].[Customers] add constraint [Сustomer_Orders] foreign key ([Orders_OrderId]) references [dbo].[Orders]([OrderId]);
