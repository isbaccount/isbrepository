﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Objects;
using System.Linq;
using ISB.Infrastructure.DAL.Interfaces;

namespace ISB.Infrastructure.DAL.Implementations.EntityFramework
{
    public class UnitOfWork : IUnitOfWork//<TContext> : IUnitOfWork where TContext : ObjectContext
    {
        private DbTransaction _transaction;
        private readonly Dictionary<Type, object> _repositories;
        //private readonly TContext _context;
        private readonly ObjectContext _context;

        public UnitOfWork()//IDatabaseFactory databaseFactory)
        {
            IDatabaseFactory databaseFactory = new DatabaseFactory(); //todo вынести и резолвить через IoC 
            _context = databaseFactory.GetObjectContext();
            //_context = Activator.CreateInstance<TContext>();
            _repositories = new Dictionary<Type, object>();
        }

        public IRepository<TSet> GetRepository<TSet>() where TSet : class
        {
            if (_repositories.Keys.Contains(typeof (TSet)))
                return _repositories[typeof (TSet)] as IRepository<TSet>;
            //var repository = new Repository<TSet, TContext>(_context);
            var repository = new Repository<TSet>(_context);
            _repositories.Add(typeof (TSet), repository);
            return repository;
        }

        /// <summary>
        /// Start Transaction
        /// </summary>
        /// <returns></returns>
        public DbTransaction BeginTransaction()
        {
            if (null == _transaction)
            {
                if (_context.Connection.State != ConnectionState.Open)
                {
                    _context.Connection.Open();
                }
                _transaction = _context.Connection.BeginTransaction();
            }
            return _transaction;
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (null != _transaction)
            {
                _transaction.Dispose();
            }

            if (null != _context)
            {
                _context.Dispose();
            }
        }

        #endregion

    }
}
