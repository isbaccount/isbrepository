﻿using System;
using System.Data.Entity;

namespace ISB.Infrastructure.DAL
{
    public class DbInitializer : DropCreateDatabaseIfModelChanges<ISBContext>
    {
        //protected override void Seed(ISBContext context)
        //{
        //    var id = Guid.NewGuid();
        //    context.Database.ExecuteSqlCommand(
        //        string.Format(
        //            "INSERT INTO  CARWASHES (CarwashId, Name ,BoxQuantity, Address, Lat,Lon) VALUES ({0}, 'Vovan', 3, 'улица Пушкина дом 1, корп.4', 59.8755,30.2085 )",
        //            id));
        //}
    }

    //http://stackoverflow.com/questions/5288996/database-in-use-error-with-entity-framework-4-code-first
    //public class ForceDeleteInitializer : IDatabaseInitializer<ISBContext>
    //{
    //    private readonly IDatabaseInitializer<ISBContext> _initializer;

    //    public ForceDeleteInitializer(IDatabaseInitializer<ISBContext> innerInitializer)
    //    {
    //        _initializer = innerInitializer;
    //    }

    //    public void InitializeDatabase(ISBContext context)
    //    {
    //        context.Database.SqlCommand("ALTER DATABASE Tocrates SET SINGLE_USER WITH ROLLBACK IMMEDIATE");
    //        _initializer.InitializeDatabase(context);
    //    }
    //}
}