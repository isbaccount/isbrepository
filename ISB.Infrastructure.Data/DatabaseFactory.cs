﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;

namespace ISB.Infrastructure.DAL
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private ISBContext _dataContext;

        public DatabaseFactory()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ISBContext>());//new CreateDatabaseIfNotExists<ISBContext>());
            _dataContext = Get();
            _dataContext.Database.Initialize(true);
        }

        public ISBContext Get()
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists<ISBContext>());
            return _dataContext ?? (_dataContext = new ISBContext("ISBConnection"));
        }

        public ObjectContext GetObjectContext()
        {
            var context = Get();  // проверить , если не работает то new ISBContext("ISBConnection")
            var adapter = (IObjectContextAdapter)context;
            return adapter.ObjectContext;
        }
    }
}