﻿using System;
using System.Collections.Generic;
using ISB.Domain.Model;
using Microsoft.AspNet.SignalR.Client.Hubs;

namespace ISB.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //e74379a4-fc97-4bc6-b986-b7fc9f47ddef

            var connection = new HubConnection("http://localhost:4702/");
            var game = connection.CreateHubProxy("carwash");

            connection.Start().Wait();

            game.Invoke("RefreshCarwashes").Wait();

            //game.Invoke<IEnumerable<Carwash>>("GetAllCarwashes")
            //   .ContinueWith(t =>
            //   {
            //       foreach (var carwash in t.Result)
            //       {
            //           System.Console.WriteLine("{0}, {1} , {2}", carwash.CarwashId, carwash.BoxQuantity, carwash.Name);
            //       }
            //   });

            //System.Console.WriteLine("next");
            //System.Console.ReadLine();

            //var newCarwash = new Carwash
            //    {
            //        CarwashId = Guid.NewGuid(),
            //        Name = "Скаут",
            //        BoxQuantity = 1,
            //        Address = "Большой Сампсониевский проспект, 66",
            //        Lat = 59.979039,
            //        Lon = 30.337009
            //    };
            //game.Invoke<bool>("CreateNewCarwash", newCarwash)
            //    .ContinueWith(t => System.Console.WriteLine("Status: {0}", t.Result));

            //game.Invoke<IEnumerable<Carwash>>("GetAllCarwashes")
            //   .ContinueWith(t =>
            //   {
            //       foreach (var carwash in t.Result)
            //       {
            //           System.Console.WriteLine("{0}, {1} , {2}", carwash.CarwashId, carwash.BoxQuantity, carwash.Name);
            //       }
            //   });

            System.Console.ReadLine();
        }
    }
}
