﻿using System.Data.Entity.ModelConfiguration;

namespace ISB.Domain.Entities.Mapping
{
    public class СustomerMapping : EntityTypeConfiguration<Сustomer>
    {
        public СustomerMapping()
        {
            HasKey(i => i.CustomerId);

            Property(i => i.FirstName);
            Property(i => i.GosNumber);
            Property(i => i.LastName);

            HasMany(i => i.Orders)
                .WithRequired();

            ToTable("Customers");   
        }
    }
}
