﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISB.Domain.Entities.Mapping
{
    class ConcreteCarwashWorkMapping : EntityTypeConfiguration<ConcreteCarwashWork>
    {
        public ConcreteCarwashWorkMapping()
        {
            HasKey(i => i.ConcreteCarwashWorkId);

            //HasRequired(i=>i.Carwash).
            Property(i => i.ConcreteCarwashWorkId)
                           .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(i => i.Duration);
            Property(i => i.Cost);

            ToTable("ConcreteCarwashWorks");
        }
    }
}
