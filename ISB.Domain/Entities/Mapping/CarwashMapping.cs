﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISB.Domain.Entities.Mapping
{
    public class CarwashMapping : EntityTypeConfiguration<Carwash>
    {
        public CarwashMapping()
        {
            HasKey(i => i.CarwashId);

            Property(i => i.CarwashId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(i => i.BoxQuantity);
            Property(i => i.Name);

            HasMany(i => i.ConcreteCarwashWorks)
                .WithRequired(i => i.Carwash)
                .HasForeignKey(i => i.CarwashId)
                .WillCascadeOnDelete();

            ToTable("Carwashes");
        }
    }
}
