using System;

namespace ISB.Domain.Entities
{
    /// <summary>
    /// �����
    /// </summary>
    public class Order
    {
        #region Primary Key

        public Guid OrderId { get; set; }

        #endregion //Primary Key

        #region Foreign Keys

        public Guid �ustomerId { get; set; }
        public Guid CarwashId { get; set; }
        public Guid WorksSetId { get; set; }

        #endregion //Foreign Keys

        /// <summary>
        /// ��� ������
        /// </summary>
        public VehicleType VehicleType { get; set; }

        /// <summary>
        /// ����� �����
        /// </summary>
        public int BoxNumber { get; set; }

        /// <summary>
        /// ���� � ����� ������
        /// </summary>
        public DateTime OrderDateTime { get; set; }

        /// <summary>
        /// ����� �������� ������
        /// </summary>
        public DateTime RaisedDateTime { get; set; }

        public virtual �ustomer �ustomer { get; set; }
        public virtual Carwash Carwash { get; set; }
        public virtual WorksSet WorksSet { get; set; }
    }
}