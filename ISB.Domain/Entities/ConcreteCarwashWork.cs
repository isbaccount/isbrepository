﻿using System;

namespace ISB.Domain.Entities
{
    /// <summary>
    /// Работа на мойке
    /// </summary>
    public class ConcreteCarwashWork
    {
        #region Primary Key

        public Guid ConcreteCarwashWorkId { get; set; }

        #endregion //Primary Key

        #region Foreign Keys

        public Guid CarwashId { get; set; }

        #endregion //Foreign Keys

        /// <summary>
        /// Тип работ
        /// </summary>
        public WorkType WorkType { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public float Cost { get; set; }

        /// <summary>
        /// Длительность
        /// </summary>
        public TimeSpan Duration { get; set; }

        public virtual Carwash Carwash { get; set; }
    }
}
