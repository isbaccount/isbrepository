using System;

namespace ISB.Domain.Entities
{
    /// <summary>
    /// ����� �����
    /// </summary>
    public class WorksSet
    {
        #region Primary Key

        public Guid WorksSetId { get; set; }

        #endregion //Primary Key

        /// <summary>
        /// ����� ������ �����
        /// </summary>
        public double Works { get; set; }
    }
}