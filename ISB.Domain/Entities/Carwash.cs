using System;
using System.Collections.Generic;

namespace ISB.Domain.Entities
{
    /// <summary>
    /// ���������
    /// </summary>
    public class Carwash
    {
        #region Primary Key

        public Guid CarwashId { get; set; }

        #endregion //Primary Key

        /// <summary>
        /// ��������
        /// </summary>
        public string Name { get; set; }

        // ����������
        // public Location Location { get; set; }

        /// <summary>
        /// ���������� ������
        /// </summary>
        public int BoxQuantity { get; set; }

        public virtual ICollection<ConcreteCarwashWork> ConcreteCarwashWorks { get; set; }
    }
}