﻿using System;
using System.Collections.Generic;

namespace ISB.Domain.Entities
{
    /// <summary>
    /// Заказчик 
    /// </summary>
    public class Сustomer
    {
        #region Primary Key

        public Guid CustomerId { get; set; }

        #endregion //Primary Key

        /// <summary>
        /// Госномер
        /// </summary>
        public string GosNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
