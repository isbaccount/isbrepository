using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ISB.Domain.Abstract;

namespace ISB.Domain.Model
{
    /// <summary>
    /// ���������
    /// </summary>
    public class Carwash : IEntity<Carwash>
    {
        #region Primary Key

        public Guid CarwashId { get; set; }

        #endregion //Primary Key

        /// <summary>
        /// �������� ��������
        /// </summary>
        public string CompanyName { get; set; } 

        /// <summary>
        /// �������� �����
        /// </summary>
        public string CarwashName { get; set; }

        // ����������
        // public Location Location { get; set; }

        /// <summary>
        /// ���������� ������
        /// </summary>
        public int BoxQuantity { get; set; }

        public string Address { get; set; }

        /// <summary>
        /// ������
        /// </summary>
        public double Lat { get; set; }

        /// <summary>
        /// �������
        /// </summary>
        public double Lon { get; set; }

        public string PhoneNumber { get; set; }

        /// <summary>
        /// ���� � ��������
        /// </summary>
        public string ImagePath { get; set; }

        /// <summary>
        /// ����� ������ ������ �����
        /// </summary>
        public DateTime BeginDatetime { get; set; }

        /// <summary>
        /// ����� ��������� ������ �����
        /// </summary>
        public DateTime EndDatetime { get; set; }

        public virtual ICollection<ConcreteCarwashWork> ConcreteCarwashWorks { get; set; }
        public virtual ICollection<Order> Orders { get; set; }

        public Carwash()
        {
            ConcreteCarwashWorks = new List<ConcreteCarwashWork>();
            Orders = new List<Order>();
        }

        public Carwash ToSerializable()
        {
            return new Carwash()
            {
                CarwashId = this.CarwashId,
                CompanyName = this.CompanyName,
                CarwashName = this.CarwashName,
                BoxQuantity = this.BoxQuantity,
                Address = this.Address,
                Lat = this.Lat,
                Lon = this.Lon,
                ConcreteCarwashWorks = this.ConcreteCarwashWorks,
                Orders = this.Orders,
                PhoneNumber = this.PhoneNumber,
                ImagePath = this.ImagePath,
                BeginDatetime = this.BeginDatetime,
                EndDatetime = this.EndDatetime
            };
        }
    }
}