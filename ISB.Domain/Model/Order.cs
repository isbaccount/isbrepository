using System;
using ISB.Domain.Abstract;

namespace ISB.Domain.Model
{
    /// <summary>
    /// �����
    /// </summary>
    public class Order : IEntity<Order>
    {
        #region Primary Key

        public Guid OrderId { get; set; }

        #endregion //Primary Key

        #region Foreign Keys

        public Guid �ustomerId { get; set; }
        public Guid CarwashId { get; set; }

        #endregion //Foreign Keys

        /// <summary>
        /// ��� ������
        /// </summary>
        public VehicleType VehicleType { get; set; }

        /// <summary>
        /// ����� �����
        /// </summary>
        public int BoxNumber { get; set; }

        /// <summary>
        /// ���� � ����� ������ ������
        /// </summary>
        public DateTime BeginOrderDateTime { get; set; }

        /// <summary>
        /// ���� � ����� ��������� ������
        /// </summary>
        public DateTime EndOrderDateTime { get; set; }

        public OrderStatus OrderStatus { get; set; }

        /// <summary>
        /// ����� �����
        /// </summary>
        public int WorksSet { get; set; }

        /// <summary>
        /// ����� �������� ������
        /// </summary>
        public DateTime RaisedDateTime { get; set; }

        public virtual �ustomer �ustomer { get; set; }
        public virtual Carwash Carwash { get; set; }

        public Order ToSerializable()
        {
            return null;//throw new NotImplementedException();
        }
    }
}