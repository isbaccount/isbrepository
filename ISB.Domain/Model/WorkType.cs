﻿using System;
using System.Reflection;
using ISB.Common;

namespace ISB.Domain.Model
{
    /// <summary>
    /// Тип работ
    /// </summary>
    [Flags]
    public enum WorkType
    {                
        [Description("Мойка кузова")]
        BodyWash = 1,
        [Description("Химчистка салона")]
        CleaningInterior = 2,        
        [Description("Полировка")]
        Polish = 4,
        [Description("Бесконтактная мойка")]
        TouchlessWash = 8,
        [Description("Ручная мойка")]        
        HandWash = 16,
        [Description("Наномойка")]
        Nanomoyka = 32,
        [Description("Удаление мелких царапин")]
        RemovingSmallScratches = 64
    }

}
