﻿namespace ISB.Domain.Model
{
    public enum OrderStatus
    {
        None = 0, // todo продумать статусы
        InProgress,
        Removed,
        Done
    }
}
