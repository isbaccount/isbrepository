﻿using System;
using System.Collections.Generic;
using ISB.Domain.Abstract;

namespace ISB.Domain.Model
{
    /// <summary>
    /// Заказчик 
    /// </summary>
    public class Сustomer : IEntity<Сustomer>
    {
        #region Primary Key

        public Guid CustomerId { get; set; }

        #endregion //Primary Key

        /// <summary>
        /// Госномер
        /// </summary>
        public string GosNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        /// <summary>
        /// Номер телефона для связи
        /// </summary>
        public string PhoneNumber { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public Сustomer()
        {
            Orders = new List<Order>();
        }

        public Сustomer ToSerializable()
        {
            return null;
        }
    }
}
