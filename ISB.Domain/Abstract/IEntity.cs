﻿namespace ISB.Domain.Abstract
{
    public interface IEntity<TEntity>
    {
        TEntity ToSerializable();
    }
}
