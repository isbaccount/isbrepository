﻿using System.Windows;
using ISB.Wpf.ViewModel;

namespace ISB.Dispatcher.Wpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public CarwashViewModel CarwashViewModel
        {
            get
            {
                var window = MainWindow as MainWindow;
                return window.CarwashViewModel;
            }
        }
    }
}
