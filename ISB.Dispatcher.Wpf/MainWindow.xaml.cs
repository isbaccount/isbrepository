﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ISB.Wpf.Model.Provider;
using ISB.Wpf.ViewModel;

namespace ISB.Dispatcher.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// The customer viewmodel.
        /// </summary>
        public CarwashViewModel CarwashViewModel { private set; get; }

        public MainWindow()
        {
            InitializeComponent();

            CarwashViewModel = new CarwashViewModel(new Provider());
        }

        private void CreateCarwashCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;//!ViewModel.IsLoaded;
        }

        private void CreateCarwashCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var window = new WindowRegisterCarwash
                {
                    Owner = this,
                    IsNewCarwash = true
                };

            if (window.ShowDialog() == true)
            {
                //TextBlockAnnouncement.Visibility = Visibility.Collapsed;

                //Cursor = Cursors.Wait;
                //ViewModel.LoadCustomers();
                //Cursor = Cursors.Arrow;

                CommandManager.InvalidateRequerySuggested();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
