﻿using System.Windows;
using System.Windows.Input;
using ISB.Wpf.ViewModel;

namespace ISB.Dispatcher.Wpf
{
    /// <summary>
    /// Interaction logic for WindowRegisterCarwash.xaml
    /// </summary>
    public partial class WindowRegisterCarwash : Window
    {
        private string _originalAddress;
        private string _originalCompanyName;
        private string _originalPhoneNumber;

        /// <summary>
        /// Flag indicating new carwash or not.
        /// </summary>
        public bool IsNewCarwash { get; set; }

        public WindowRegisterCarwash()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Helper. Makes it easy to get to customer ViewModel.
        /// </summary>
        private CarwashViewModel CarwashViewModel
        {
            get { return (Application.Current as App).CarwashViewModel; }
        }

        /// <summary>
        /// Loads new or existing record.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CommandModel commandModel;

            // New customer.
            if (IsNewCarwash)
            {

                this.DataContext = CarwashViewModel.NewCarwashModel;
                Title = "Add new carwash";

                commandModel = CarwashViewModel.AddCommandModel;

                // Display little hint message
                LabelNewMessage1.Visibility = Visibility.Visible;
                LabelNewMessage2.Visibility = Visibility.Visible;
            }
            else
            {
                this.DataContext = CarwashViewModel.CurrentCustomer;

                // Save off original values. Due to binding viewmodel is changed immediately when editing.
                // So, when canceling we have these values to restore original state.
                // Suggestion: could be implemented as Memento pattern.
                _originalAddress = CarwashViewModel.CurrentCustomer.Address;
                _originalCompanyName = CarwashViewModel.CurrentCustomer.CompanyName;
                _originalPhoneNumber = CarwashViewModel.CurrentCustomer.PhoneNumber;

                Title = "Edit customer";

                commandModel = CarwashViewModel.EditCommandModel;
            }

            textBoxCustomer.Focus();

            // The command helps determine whether save button is enabled or not
            buttonSave.Command = commandModel.Command;
            buttonSave.CommandParameter = this.DataContext;
            buttonSave.CommandBindings.Add(new CommandBinding(commandModel.Command, commandModel.OnExecute, commandModel.OnCanExecute));
        }

        // Save button was clicked
        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            Close();
        }

        // Cancel button was clicked
        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            // Restore viewmodel to original values
            if (!IsNewCarwash)
            {
                CarwashViewModel.CurrentCustomer.Address = _originalAddress;
                CarwashViewModel.CurrentCustomer.CompanyName = _originalCompanyName;
                CarwashViewModel.CurrentCustomer.PhoneNumber = _originalPhoneNumber;
            }
        }
    }
}
