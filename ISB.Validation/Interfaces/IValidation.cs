﻿namespace ISB.Validation.Interfaces
{
    public interface IValidation<in TEntity> where TEntity : class
    {
        void EnsureValidForCreation(TEntity entity);
    }
}
