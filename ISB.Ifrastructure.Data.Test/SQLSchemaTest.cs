﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Xml;
using ISB.Domain.Model;
using ISB.Infrastructure.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ISB.Ifrastructure.Data.Test
{
    [TestClass]
    public class SQLSchemaTest
    {
        ISBContext _context;

        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<ISBContext>());
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _context = new ISBContext("ISBConnection");
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _context.Dispose();
        }

        [TestMethod]
        public void SchemaCreationTest()
        {
            _context.Database.Initialize(true);
        }

        [TestMethod]
        public void WriteEdmxFileTest()
        {
            var settings = new XmlWriterSettings { Indent = true };

            using (XmlWriter writer = XmlWriter.Create(@"..\..\..\ISB.Infrastructure.Data\Model.edmx", settings))
            {
                EdmxWriter.WriteEdmx(_context, writer);
            }
        }

        [TestMethod]
        public void CreateDatabaseScriptTest()
        {
            string script = ((IObjectContextAdapter)_context).ObjectContext.CreateDatabaseScript();

            using (var writer = new StreamWriter(@"..\..\..\ISB.Infrastructure.Data\DbScript.sql"))
            {
                writer.Write(script);
            }
        }
    }
}
