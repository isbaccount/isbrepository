﻿using System;
using System.Windows.Input;
using ISB.Wpf.Model.Provider;
using ISB.Wpf.Model.ViewModelObjects;

namespace ISB.Wpf.ViewModel
{
    public class CarwashViewModel: ViewModelBase
    {
        private IProvider _provider;

        /// <summary>
        /// Observable collection of customers.
        /// </summary>
        //public ObservableCollection<CarwashModel> Customers { private set; get; }
        public CarwashModel Carwash { private set; get; }

        /// <summary>
        /// Command model for adding a customer.
        /// </summary>
        public CommandModel AddCommandModel{ private set; get; }

        /// <summary>
        /// Command model for editing a customer.
        /// </summary>
        public CommandModel EditCommandModel { private set; get; }

        ///// <summary>
        ///// Command model for deletign a customer.
        ///// </summary>
        //public CommandModel DeleteCommandModel { private set; get; }

        /// <summary>
        /// Constructor. Creates a new Customer ViewModel.
        /// </summary>
        /// <param name="provider">The provider.</param>
        public CarwashViewModel(IProvider provider)
        {
            _provider = provider;

            //Customers = new ObservableCollection<CarwashModel>();

            AddCommandModel = new AddCommand(this);
            EditCommandModel = new EditCommand(this);
            //DeleteCommandModel = new DeleteCommand(this);
        }

        /// <summary>
        /// Indicates whether the customer data has been loaded.
        /// </summary>
        public bool IsLoaded { private set; get; }

        /// <summary>
        /// Gets a new customer.
        /// </summary>
        public CarwashModel NewCarwashModel
        {
            get { return new CarwashModel(_provider); }
        }

        /// <summary>
        /// Indicates whether a new customer can be added.
        /// </summary>
        public bool CanAdd
        {
            get { return IsLoaded; }
        }

        /// <summary>
        /// Indicates whether a customer is currently selected.
        /// </summary>
        public bool CanEdit
        {
            get { return IsLoaded && CurrentCustomer != null; }
        }

        /// <summary>
        /// Indicates whether a customer is selected that can be deleted.
        /// </summary>
        public bool CanDelete
        {
            get { return IsLoaded && CurrentCustomer != null; }
        }

        /// <summary>
        /// Indicates whether a customer is selected and orders can be viewed.
        /// </summary>
        public bool CanViewOrders
        {
            get { return IsLoaded && CurrentCustomer != null; }
        }

        ///// <summary>
        ///// Retrieves and displays customers in given sort order.
        ///// </summary>
        ///// <param name="sortExpression">Sort order.</param>
        //public void LoadCustomers()
        //{
        //    string sortExpression = "CompanyName ASC";
        //    foreach(var customer in _provider.GetCustomers(sortExpression))
        //        Customers.Add(customer);
            
        //    if (Customers.Count > 0)
        //        CurrentCustomer = Customers[0];

        //    IsLoaded = true;
        //}

        ///// <summary>
        ///// Clear customers from display.
        ///// </summary>
        //public void UnloadCustomers()
        //{
        //    Customers.Clear();

        //    CurrentCustomer = null;
        //    IsLoaded = false;
        //}

        private CarwashModel _currentCustomerModel;
        public CarwashModel CurrentCustomer
        {
            get { return _currentCustomerModel; }
            set
            {
                if (_currentCustomerModel != value)
                {
                    _currentCustomerModel = value;
                    OnPropertyChanged("CurrentCustomer");
                }
            }
        }

        #region Private Command classes

        /// <summary>
        /// Private implementation of the Add Command.
        /// </summary>
        private class AddCommand : CommandModel
        {
            private CarwashViewModel _viewModel;

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="viewModel"></param>
            public AddCommand(CarwashViewModel viewModel)
            {
                _viewModel = viewModel;
            }

            public override void OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
            {
                var carwash = e.Parameter as CarwashModel;
                
                // Check that all values have been entered.
                e.CanExecute =
                    (!string.IsNullOrEmpty(carwash.Address)
                  && !string.IsNullOrEmpty(carwash.CarwashName)
                  && !string.IsNullOrEmpty(carwash.PhoneNumber));

                e.Handled = true;
            }

            public override void OnExecute(object sender, ExecutedRoutedEventArgs e)
            {
                var carwash = e.Parameter as CarwashModel;
                carwash.Add();

                _viewModel.Carwash = carwash;
                //_viewModel.CurrentCustomer = carwash;
            }
        }

        /// <summary>
        /// Private implementation of the Edit command
        /// </summary>
        private class EditCommand : CommandModel
        {
            private CarwashViewModel _viewModel;

            public EditCommand(CarwashViewModel viewModel)
            {
                _viewModel = viewModel;
            }

            public override void OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
            {
                var customer = e.Parameter as CarwashModel;

                // Check that all values have been set
                e.CanExecute = (customer.CarwashId != Guid.Empty
                  && !string.IsNullOrEmpty(customer.Address)
                  && !string.IsNullOrEmpty(customer.CompanyName)
                  && !string.IsNullOrEmpty(customer.PhoneNumber));

                e.Handled = true;
            }

            public override void OnExecute(object sender, ExecutedRoutedEventArgs e)
            {
                var customerModel = e.Parameter as CarwashModel;
                customerModel.Update();
            }
        }

        ///// <summary>
        ///// Private implementation of the Delete command
        ///// </summary>
        //private class DeleteCommand : CommandModel
        //{
        //    private CarwashViewModel _viewModel;

        //    public DeleteCommand(CarwashViewModel viewModel)
        //    {
        //        _viewModel = viewModel;
        //    }

        //    public override void OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        //    {
        //        e.CanExecute = _viewModel.CanDelete;
        //        e.Handled = true;
        //    }

        //    public override void OnExecute(object sender, ExecutedRoutedEventArgs e)
        //    {
        //        var customerModel = _viewModel.CurrentCustomer;

        //        if (customerModel.Delete() > 0)
        //        {
        //            _viewModel.Customers.Remove(customerModel);
        //            e.Handled = true;
        //        }
        //    }
        //}

        #endregion
    }
}
