﻿using System;
using System.Collections.Generic;

namespace ISB.Common
{
    public static class CustomCollectionExtentions
    {
        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach (T item in enumeration)
            {
                action(item);
            }
        }
    }
}
