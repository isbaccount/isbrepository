﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISB.Domain.Model;
using ISB.Services;
using ISB.Validation.Interfaces;
using ISB.Common;

namespace ISB.Presentation.WebUI.Controllers
{
    public class HomeController : Controller
    {
        List<SelectListItem> required = new List<SelectListItem>();

        public ActionResult Index()
        {
            
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            foreach (WorkType workType in Enum.GetValues(typeof(WorkType)))
            {                
                var valueEnum = (int)workType;
                required.Add(new SelectListItem { Value = valueEnum.ToString(), Text = workType.GetDescription() });
            }

            /*required.Add(new SelectListItem { Value = "1", Text = "Мойка кузова" });
            required.Add(new SelectListItem { Value = "2", Text = "Химчистка салона" });
            required.Add(new SelectListItem { Value = "3", Text = "Полировка" });
            required.Add(new SelectListItem { Value = "4", Text = "Воскование" });*/
            ViewData["required"] = required;

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        /// <summary>
        /// Ajax метод для резервации места на мойке
        /// </summary>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult ReserveWash(string idWash, string phoneNumber, int typeWorks, string gosNumber, string beginDt)
        {
            var vv = DateTime.Parse(beginDt);
            var vv2 = (WorkType)typeWorks;

            IValidation<Сustomer> validation = null;
            var customerService = new CustomerService(validation);
            var customer = new Сustomer { GosNumber = gosNumber, PhoneNumber = phoneNumber }; // todo вынести в фабричный метод
            var c = customerService.GetСustomerIfExist(customer);

            IValidation<Order> orderValidation = null;
            var orderService = new OrderService(orderValidation);

            var order = new Order
            {
                СustomerId = customer.CustomerId,
                CarwashId = Guid.Parse(idWash),
                WorksSet = typeWorks,
                BeginOrderDateTime = DateTime.Now,
                EndOrderDateTime = DateTime.Now.AddHours(1),
                BoxNumber = 3,
                OrderStatus = OrderStatus.InProgress,
                RaisedDateTime = DateTime.Now,
                VehicleType = VehicleType.Coupe,
            };
            orderService.Create(order);

            //string idWash = Request.Params[0] as string;
            //string idClient = Request.Params[1] as string;
            return Json("зарезервировано");
        }

        /// <summary>
        /// Ajax метод для поиска свободных моек
        /// </summary>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult FindFreeWash(string bnamePar)
        {
            IValidation<Carwash> validation = null;
            var carwashService = new CarwashService(validation);
            var washes = carwashService
                .GetAllSerialized().Select(i => new
                {
                    Lat = i.Lat,
                    Lon = i.Lon,
                    CarwashId = i.CarwashId,
                    CarwashName = i.CarwashName,
                    PhoneNumber = i.PhoneNumber,
                    Address = i.Address,
                    Duration = "Duration",
                    Price = 400,
                    PathImage = string.Empty,
                    Time = "time",
                });

            //var wash1 = new Carwash { CarwashId = Guid.NewGuid(), CarwashName = "Мойка 1" , Address = "улица Пушкина дом 1, корп.4", Lat = 59.8755, Lon = 30.2085 };
            //var wash2 = new Carwash { CarwashId = Guid.NewGuid(), CarwashName = "Мойка 2", Address = "улица Тургнева дом 23", Lat = 59.910, Lon = 30.347 };
            //var wash3 = new Carwash { CarwashId = Guid.NewGuid(), CarwashName = "Мойка 3", Address = "улица Тургнева дом 23", Lat = 59.880, Lon = 30.2175 };
            //var washes = new List<Carwash> { wash1, wash2, wash3 };

            return Json(new { washes });
        }
    }
}
