﻿
var nav = null;
var myLat, myLon;
var myMap;
var myCollection;

if (nav == null) {
    nav = window.navigator;
}

var geoloc = nav.geolocation;
if (geoloc != null) {
    geoloc.getCurrentPosition(successCallback, errorCallback);
}

function successCallback(position) {

    myLat = position.coords.latitude;
    myLon = position.coords.longitude;
    loadMap();
}

function errorCallback(error) {
    //document.getElementById("status").innerHTML = strMessage;
}

function loadMap() {

    ymaps.ready(init);

    function init() {
        myMap = new ymaps.Map("map", {
            center: [myLat, myLon],
            zoom: 12
        });

        myPlacemarkCurrent = new ymaps.Placemark([myLat, myLon]);

        myCollection = new ymaps.GeoObjectCollection();

        myMap.geoObjects.add(myPlacemarkCurrent);
        myMap.controls.add('zoomControl').add('typeSelector').add('mapTools');
        // Включим масштабирование колесом мыши
        myMap.behaviors.enable('scrollZoom');
    }
}

function findFreeWash() {

    $.ajax({
        type: 'POST',
        url: '/Home/FindFreeWash',
        data: { bnamePar: '123dd' },
        dataType: 'json',
        success: function (responseData) {

            mapRefresh(responseData.washes);
        },
        error: function (responseData) {
            alert('error');
        }
    });
}

function mapRefresh(washes) {

    myCollection.removeAll();
    myCollection = new ymaps.GeoObjectCollection();
    // Создаем шаблон для отображения контента балуна
    var myBalloonLayout = ymaps.templateLayoutFactory.createClass(
    '<img src="$[properties.picturePath]" width="210" height="120"/>' +
            '<h3 id="idWashBaloon" style="display:none">$[properties.idWash]</h3>' +
            '<h3>$[properties.name]</h3>' +
            '<label id="phoneNumberBaloon" for="required">Телефон: $[properties.telephone]</label><br/>' +
            '<label for="required">Адрес: $[properties.address]</label><br/><br/>' +
            '<label for="required">Время мойки: $[properties.time]</label><label for="timeSpan"></label><br/>' +
            '<label for="required">Длительность работы: $[properties.duration] мин.</label><label for="timeSpan"></label><br/>' +
            '<label for="required">Цена: $[properties.price] руб.</label><label for="price"></label><br/><br/>' +
            '<label id="gosNumberBaloon" for="required">Гос номер:</label>&nbsp;&nbsp;<input id="gosNumber" type="text" name="fname" style="width:75px;"><br/>' +
            '<label for="required">Номер моб. тел.:</label>&nbsp;&nbsp;<input id="telefoneNumber" type="text" name="fname" style="width:115px;"><br/>' +
            '<button class="k-button" id="get" onclick="reserve()">Забронировать</button>'
        );
    // Помещаем созданный шаблон в хранилище шаблонов. Теперь наш шаблон доступен по ключу 'my#superlayout'.
    ymaps.layout.storage.add('my#superlayout', myBalloonLayout);
    myCollection.options.set({
        balloonContentBodyLayout: 'my#superlayout',
        // Максимальная ширина балуна в пикселах
        balloonMaxWidth: 300
    });

    $.each(washes, function (index) {

        myPlacemarkWash = new ymaps.Placemark([washes[index].Lat, washes[index].Lon],
        {
            idWash: washes[index].CarwashId,
            name: washes[index].CarwashName,
            telephone: washes[index].PhoneNumber,
            address: washes[index].Address,
            duration: washes[index].Duration,
            price: washes[index].Price,
            picturePath: washes[index].PathImage,
            time: washes[index].Time
        },
        {
            iconImageHref: '../Content/images/carwash.png', // картинка иконки
            iconImageSize: [30, 42], // размеры картинки
            iconImageOffset: [-3, -42] // смещение картинки                    
        });

        myCollection.add(myPlacemarkWash);
    });
    myMap.geoObjects.add(myCollection);
}

function reserve() {

    var hour = $("#timeLabelH").text();
    var minute = $("#timeLabelM").text();
    var dateBegin = $("#datepicker").val() + " " + hour + ":" + minute + ":00";
    var multiselect = $("#required").data("kendoMultiSelect");
    var value = multiselect.value();
    var countWork = value.length;
    
    var resultWorks=0;
    for(var i=0; i<countWork; i++)
    {
        resultWorks += parseInt(value[i]);
    }

    $.ajax({
        type: 'POST',
        url: '/Home/ReserveWash',
        data: { idWash: $("#idWashBaloon").text(), phoneNumber: $("#telefoneNumber").val(), typeWorks: resultWorks, gosNumber: $("#gosNumber").val(), beginDt: dateBegin },
        dataType: 'json',
        success: function (responseData) {
            alert(responseData);
        },
        error: function (responseData) {
            alert('error');
        }
    });
}