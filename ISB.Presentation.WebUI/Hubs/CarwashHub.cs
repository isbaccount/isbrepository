﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using ISB.Domain.Model;
using ISB.Services;
using ISB.Validation.Interfaces;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace ISB.Presentation.WebUI.Hubs
{
    [HubName("carwash")]
    public class CarwashHub : Hub
    {
        public Task<IEnumerable<Carwash>> GetAllCarwashes()
        {
            return Task.Factory.StartNew(() =>
            {
                IValidation<Carwash> validation = null;
                var carwashService = new CarwashService(validation);
                IEnumerable<Carwash> result = carwashService.GetAllSerialized();
                return result;
            });
        }

        public Task<bool> CreateNewCarwash(Carwash carwash)
        {
            return Task.Factory.StartNew(() =>
                {
                    IValidation<Carwash> validation = null;
                    var carwashService = new CarwashService(validation);
                    bool result = carwashService.Create(carwash);
                    if (result)
                        Clients.All.findFreeWashes();
                    return result;
                });
        }

        public void RefreshCarwashes()
        {
            //Clients.All.testMethod();
            Clients.All.findFreeWashes();
        }
    }
}