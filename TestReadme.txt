Open SQLSchemaTest class and run SchemaCreationTest method to create the database for the model. 
Once you do that, you can run WriteEdmxFileTest and CreateDatabaseScriptTest methods to generate 
the EDM and SQL DDL respectively.