﻿using System;
using System.Threading.Tasks;
using ISB.Wpf.Model.Provider;

namespace ISB.Wpf.Model.ViewModelObjects
{
    public class CarwashModel : BaseModel
    {
        private readonly IProvider _provider;

        private readonly Guid _carwashId;
        private string _companyName;
        private string _address;
        private string _phoneNumber;
        private string _carwashName;
        private int _boxQuantity;
        private double _lat;
        private double _lon;
        private DateTime _beginDatetime;
        private DateTime _endDatetime;

        //private ObservableCollection<OrderModel> _orders;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="provider">The provider for the customer.</param>
        public CarwashModel(IProvider provider)
        {
            _provider = provider;
            _carwashId = Guid.NewGuid();
        }
        /// <summary>
        /// Adds a new customer.
        /// </summary>
        public Task<bool> Add()
        {
            return _provider.AddCustomer(this);
        }

        ///// <summary>
        ///// Deletes a customer.
        ///// </summary>
        //public int Delete()
        //{
        //    var orders = _provider.GetOrders(this.CustomerId);
        //    if (orders == null || orders.Count == 0)
        //        return _provider.DeleteCustomer(this.CustomerId);
        //    else
        //        return 0; // Nothing deleted because customer has orders.

        //}

        /// <summary>
        /// Updates a customer.
        /// </summary>
        public int Update()
        {
            return 999;//_provider.UpdateCustomer(this);
        }

        public int BoxQuantity  {
           get {
                return _boxQuantity;
            }
            set
            {
                if (_boxQuantity != value)
                {
                    _boxQuantity = value;
                    Notify("BoxQuantity");
                }
            }
        }

        /// <summary>
        /// Широта
        /// </summary>
        public double Lat
        {
            get
            {
                return _lat;
            }
            set
            {
                if (_lat != value)
                {
                    _lat = value;
                    Notify("Lat");
                }
            }
        }
        /// <summary>
        /// Долгота
        /// </summary>
        public double Lon
        {
            get
            {
                return _lon;
            }
            set
            {
                if (_lon != value)
                {
                    _lon = value;
                    Notify("Lon");
                }
            }
        }

       

        ///// <summary>
        ///// Gets or sets customerId
        ///// </summary>
        public Guid CarwashId
        {
            get
            {
                return _carwashId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CarwashName
        {
            get
            {
                return _carwashName;
            }
            set
            {
                if (_carwashName != value)
                {
                    _carwashName = value;
                    Notify("CarwashName");
                }
            }
        }

        /// <summary>
        /// Gets or sets customer name.
        /// </summary>
        public string CompanyName
        {
            get
            {
                return _companyName;
            }
            set
            {
                if (_companyName != value)
                {
                    _companyName = value;
                    Notify("CompanyName");
                }
            }
        }


        /// <summary>
        /// Время начала работы мойки
        /// </summary>
        public DateTime BeginDateTime
        {
            get
            {
                return _beginDatetime;
            }
            set
            {
                if (_beginDatetime != value)
                {
                    _beginDatetime = value;
                    Notify("BeginDateTime");
                }
            }
        }
        /// <summary>
        /// Время окончания работы мойки
        /// </summary>
        public DateTime EndDateTime
        {
            get
            {
                return _endDatetime;
            }
            set
            {
                if (_endDatetime != value)
                {
                    _endDatetime = value;
                    Notify("EndDateTime");
                }
            }
        }


        /// <summary>
        /// Gets or sets customer city.
        /// </summary>
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                if (_address != value)
                {
                    _address = value;
                    Notify("Address");
                }
            }
        }

        /// <summary>
        /// Gets or set customer country.
        /// </summary>
        public string PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }
            set
            {
                if (_phoneNumber != value)
                {
                    _phoneNumber = value;
                    Notify("PhoneNumber");
                }
            }
        }

        ///// <summary>
        ///// Gets or sets list of orders associated with customer.
        ///// </summary>
        //public ObservableCollection<OrderModel> Orders
        //{
        //    get {  LazyloadOrders(); return _orders; }
        //    set {  _orders = value; Notify("Orders"); }
        //}

        // Private helper that performs lazy loading of orders.
        //private void LazyloadOrders()
        //{
        //    if (_orders == null)
        //    {
        //        Orders = _provider.GetOrders(this.CustomerId) ?? new ObservableCollection<OrderModel>();
        //    }
        //}
    }
}
