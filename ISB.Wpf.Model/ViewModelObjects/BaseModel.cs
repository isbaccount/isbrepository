﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISB.Wpf.Model.ViewModelObjects
{
    /// <summary>
    /// Abstract base class for business object models. 
    /// </summary>
    /// <remarks>
    /// Methods ensure that they are called on the UI thread only.
    /// </remarks>
    public abstract class BaseModel : INotifyPropertyChanged
    {
        // Dispatcher associated with model
        private PropertyChangedEventHandler _propertyChangedEvent;

        /// <summary>
        /// Constructor.
        /// </summary>
        public BaseModel()
        {
            // Save off dispatcher 
        }

        /// <summary>
        /// PropertyChanged event for INotifyPropertyChanged implementation.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged
        {
            add
            {
                _propertyChangedEvent += value;
            }
            remove
            {
                _propertyChangedEvent -= value;
            }
        }

        /// <summary>
        /// Utility function for use by subclasses to notify that a property value has changed.
        /// </summary>
        /// <param name="propertyName">The name of the property.</param>
        protected void Notify(string propertyName)
        {
            if (_propertyChangedEvent != null)
            {
                _propertyChangedEvent(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}


