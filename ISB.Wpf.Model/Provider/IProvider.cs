﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ISB.Wpf.Model.ViewModelObjects;

namespace ISB.Wpf.Model.Provider
{
    public interface IProvider
    {
        Task<bool> AddCustomer(CarwashModel customer);
    }
}
