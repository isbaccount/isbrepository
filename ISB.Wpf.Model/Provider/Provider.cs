﻿using System.Threading.Tasks;
using ISB.Wpf.Model.DataTransferObjectMapper;
using ISB.Wpf.Model.ViewModelObjects;
using Microsoft.AspNet.SignalR.Client.Hubs;

namespace ISB.Wpf.Model.Provider
{
    public class Provider : IProvider
    {
        protected HubConnection Connection;

        public Provider()
        {
            Connection = new HubConnection("http://localhost:4702/");
        }

        public async Task<bool> AddCustomer(CarwashModel carwash)
        {
            var carwashHub = Connection.CreateHubProxy("carwash");
            var newCarwash = Mapper.ToDataTransferObject(carwash);
            Connection.Start().Wait();
            return await carwashHub.Invoke<bool>("CreateNewCarwash", newCarwash).ContinueWith(i =>
                {
                    carwashHub.Invoke("RefreshCarwashes").Wait();
                    Connection.Stop(); //todo правильно ли закрывать connection
                    return true;
                });
        }
    }
}
