﻿using ISB.Domain.Model;
using ISB.Wpf.Model.ViewModelObjects;

namespace ISB.Wpf.Model.DataTransferObjectMapper
{
    internal static class Mapper
    {
        /// <summary>
        /// Maps carwash model object to carwash data transfer object.
        /// </summary>
        /// <param name="carwash">Carwash model object</param>
        /// <returns>Carwash data transfer object.</returns>
        internal static Carwash ToDataTransferObject(CarwashModel carwash)
        {
            return new Carwash
                {
                    CarwashId = carwash.CarwashId,
                    Address = carwash.Address,
                    CarwashName = carwash.CarwashName, //todo добавить CompanyName
                    BoxQuantity = carwash.BoxQuantity,
                    BeginDatetime = carwash.BeginDateTime,
                    EndDatetime = carwash.EndDateTime,
                    Lat = carwash.Lat,
                    Lon = carwash.Lon,
                    PhoneNumber = carwash.PhoneNumber
                };
        }
    }
}
