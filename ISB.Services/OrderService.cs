﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ISB.Domain.Model;
using ISB.Services.Infrastructure;
using ISB.Validation.Interfaces;

namespace ISB.Services
{
    public class OrderService : GenericService<Order>
    {
        public OrderService(IValidation<Order> validation)
            : base(validation)
        {
        }

        public override bool Create(Order entity)
        {
            entity.OrderId = Guid.NewGuid(); 
            return base.Create(entity);
        }
    }
}