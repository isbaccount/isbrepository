﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ISB.Domain.Model;
using ISB.Infrastructure.DAL.Implementations.EntityFramework;
using ISB.Services.Infrastructure;
using ISB.Validation.Interfaces;

namespace ISB.Services
{
    public class CustomerService : GenericService<Сustomer>
    {
        public CustomerService(IValidation<Сustomer> validation)
            : base(validation)
        {
        }

        public Сustomer GetСustomerIfExist(Сustomer entity)
        {
            var result = entity;
            var customerFromBase = GetGuidIfExist(entity.GosNumber);

            if (customerFromBase == null)
            {
                entity.CustomerId = Guid.NewGuid();
                Create(entity);
            }
            else
                result = customerFromBase;

            return result;
        }

        //public override bool Create(Сustomer entity)
        //{


        //    return base.Create(entity);
        //}

        public Сustomer GetGuidIfExist(string gosNumber)
        {
            Сustomer result = null;

            try
            {
                var unitOfWork = new UnitOfWork();
                var repository = unitOfWork.GetRepository<Сustomer>();

                using (unitOfWork)
                {
                    result = repository.DoQuery(new Specification<Сustomer>(i => i.GosNumber == gosNumber)).SingleOrDefault();
                }
            }

            catch (Exception ex)
            {
                //throw ErrorHandler.WrapException(ex, ErrorCodes.DB_DeleteUser_Error_Code);
            }
            return result;
        }

    }
}
