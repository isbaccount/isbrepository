﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ISB.Domain.Model;
using ISB.Infrastructure.DAL.Implementations.EntityFramework;
using ISB.Services.Infrastructure;
using ISB.Validation.Interfaces;

namespace ISB.Services
{
    public sealed class CarwashService : GenericService<Carwash>
    {
        public CarwashService(IValidation<Carwash> validation)
            : base(validation)
        {

        }

        //public override IEnumerable<Carwash> GetAll()
        //{
        //    IEnumerable<Carwash> result = null;

        //    //One oneProxy = new One { OneId = 1 };
        //    // add a few collection items
        //    //oneProxy.Two.Add(new Two() { OneId = 1, TwoId = 1, One = oneProxy});
        //    //oneProxy.Two.Add(new Two() { OneId = 1, TwoId = 2, One = oneProxy});

        //    // create a mapping (this should go in either global.asax 
        //    // or in an app_start class)
        //    //AutoMapper.Mapper.CreateMap<One, OnePoco>();
        //    //AutoMapper.Mapper.CreateMap<Two, TwoPoco>();

        //    //// do the mapping- bingo, check out the asjson now
        //    //// i.e. oneMapped.AsJson
        //    //var oneMapped = AutoMapper.Mapper.Map<One, OnePoco>(oneProxy);

        //    //return View(oneMapped);
        //    //        var all = base.GetAll();
        //    //        if (all.Any())
        //    //        {
        //    //            var t = ObjectContext.GetObjectType(all.First().GetType());
        //    //            result = all.Cast<ObjectContext.GetObjectType(all.First().GetType())>();

        //    //        }
        //    //        return all;
        //    //    }
        //}

        public Carwash GetById(Guid id)
        {
            Carwash carwash = null;

            try
            {
                var unitOfWork = new UnitOfWork();
                var repository = unitOfWork.GetRepository<Carwash>();

                using (unitOfWork)
                {
                    carwash = repository
                        .DoQuery(new Specification<Carwash>(i => i.CarwashId == id))
                        .Cast<Carwash>()
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                //throw ErrorHandler.WrapException(ex, ErrorCodes.DB_DeleteUser_Error_Code);
            }

            return carwash;
        }


    }
}
