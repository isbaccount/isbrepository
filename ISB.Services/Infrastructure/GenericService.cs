﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ISB.Domain.Abstract;
using ISB.Domain.Model;
using ISB.Infrastructure.DAL;
using ISB.Infrastructure.DAL.Implementations.EntityFramework;
using ISB.Infrastructure.DAL.Interfaces;
using ISB.Validation.Interfaces;
using ISB.Common;

namespace ISB.Services.Infrastructure
{
    public class GenericService<TEntity> where TEntity : class, IEntity<TEntity>
    {
        #region Properties
        protected IRepository<TEntity> Repository;
        protected IValidation<TEntity> Validation;
        #endregion

        #region Constructors

        public GenericService(IValidation<TEntity> validation)
        {
            Validation = validation;
        }

        //public GenericService(IRepository<TEntity> repository, IValidation<TEntity> validation)
        //{
        //    Repository = repository;
        //    Validation = validation;
        //}

        #endregion

        #region Services

        public virtual IEnumerable<TEntity> GetAll()
        {
            IEnumerable<TEntity> result = new List<TEntity>();

            try
            {
                var unitOfWork = new UnitOfWork();
                var repository = unitOfWork.GetRepository<TEntity>();

                using (unitOfWork)
                {
                    result = repository.SelectAll();
                }
            }

            catch (Exception ex)
            {
                //throw ErrorHandler.WrapException(ex, ErrorCodes.DB_DeleteUser_Error_Code);
            }
            return result;
        }

        public virtual IEnumerable<TEntity> GetAllSerialized()
        {
            IEnumerable<TEntity> result = new List<TEntity>();

            try
            {
                var unitOfWork = new UnitOfWork();
                var repository = unitOfWork.GetRepository<TEntity>();

                using (unitOfWork)
                {
                    result = repository.SelectAll().Select(i => i.ToSerializable()).ToList();
                }
            }

            catch (Exception ex)
            {
                //throw ErrorHandler.WrapException(ex, ErrorCodes.DB_DeleteUser_Error_Code);
            }
            return result;
        }

        //public virtual TEntity GetById(Guid id)
        //{
        //    var unitOfWork = new UnitOfWork();
        //    var carwashRepository = unitOfWork.GetRepository<Carwash>();

        //    //fefa697a-ab70-4c11-97e5-fbd8779025c0
        //    //carwashRepository.DoQuery(new Specification<Carwash>(i=>i.CarwashId = id)
        //    //return Repository.DoQuery(new Specification<Carwash>(e => e.CarwashId == id));//.GetByKey<TEntity>(id);
        //}

        //public virtual TEntity FindOne(ISpecification<TEntity> specification)
        //{
        //    return Repository.FindOne<TEntity>(specification);
        //}

        public virtual bool Create(TEntity entity)
        {
            var result = false;
            try
            {
                var unitOfWork = new UnitOfWork();
                var repository = unitOfWork.GetRepository<TEntity>();

                using (unitOfWork)
                {
                    using (var transaction = unitOfWork.BeginTransaction())
                    {
                        repository.Add(entity);

                        if (unitOfWork.Save() >= 0)
                        {
                            transaction.Commit();
                            result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ErrorHandler.WrapException(ex, ErrorCodes.DB_DeleteUser_Error_Code);
            }

            return result;
        }

        //public virtual void Update(TEntity entity)
        //{
        //    //Validation.EnsureValidForCreation(entity);
        //    Save();
        //}

        //public virtual void Delete(int id)
        //{
        //    var entity = Repository.GetByKey<TEntity>(id);
        //    Repository.Delete(entity);
        //    Save();
        //}

        //public virtual bool Delete(TEntity entity)
        //{


        //    try
        //    {
        //        var unitOfWork = new UnitOfWork();
        //        var carwashRepository = unitOfWork.GetRepository<Carwash>();

        //        using (unitOfWork)
        //        {
        //            using (var transaction = unitOfWork.BeginTransaction())
        //            {
        //                User UserTobeRemoved = userRepository.SelectByKey(UserID);
        //                if (UserTobeRemoved == null)
        //                    return false;

        //                userRepository.DeleteRelatedEntries(UserTobeRemoved);
        //                userRepository.Delete(UserTobeRemoved);
        //                if (unitOfWork.Save() >= 0)
        //                {
        //                    transaction.Commit();
        //                    return true;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //throw ErrorHandler.WrapException(ex, ErrorCodes.DB_DeleteUser_Error_Code);
        //    }
        //    return false;


        //    //Repository.Delete(entity);
        //    //Save();
        //}

        //public void Save()
        //{
        //    Repository.UnitOfWork.SaveChanges();
        //}
        #endregion
    }
}
